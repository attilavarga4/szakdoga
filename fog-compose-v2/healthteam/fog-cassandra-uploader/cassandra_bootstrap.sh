/opt/wait-for-it/wait-for-it.sh -t 0 $CASSANDRA:9042
echo "Sleep starts."
sleep 1m
echo "Sleep ends."
if [ $SERVER_ENV -eq 1 ]
then
  cqlsh -u cassandra -p cassandra --request-timeout="20" -f /opt/fog-cassandra-init.staging-prod.cql $CASSANDRA 9042
else
  cqlsh -u cassandra -p cassandra --request-timeout="20" -f /opt/fog-cassandra-init.cql $CASSANDRA 9042
fi
echo "Init query finished."
echo "Ready to use!"
/bin/bash
