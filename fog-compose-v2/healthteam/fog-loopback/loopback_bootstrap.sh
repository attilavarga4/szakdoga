/opt/wait-for-it/wait-for-it.sh -t 0 $CASSANDRA:9042
echo "Sleep starts."
sleep 2m
echo "Sleep ends. "
curl -i -X PUT "http://$NAMENODE:50070/webhdfs/v1/fulorrgege/Observation/test-pic.jpg?user.name=root&op=CREATE"
curl -i -X PUT -T /opt/fog-loopback-v2/server/storage/container1/test-pic.jpg "http://$DATANODE:50075/webhdfs/v1/fulorrgege/Observation/test-pic.jpg?op=CREATE&user.name=root&namenoderpcaddress=$NAMENODE:8020&overwrite=false"
curl -i -X PUT "http://$NAMENODE:50070/webhdfs/v1/fulorrgege/Observation/test-pic1.jpg?user.name=root&op=CREATE"
curl -i -X PUT -T /opt/fog-loopback-v2/server/storage/container1/test-pic1.jpg "http://$DATANODE:50075/webhdfs/v1/fulorrgege/Observation/test-pic1.jpg?op=CREATE&user.name=root&namenoderpcaddress=$NAMENODE:8020&overwrite=false"
curl -i -X PUT "http://$NAMENODE:50070/webhdfs/v1/fulorrgege/Observation/test-video.mov?user.name=root&op=CREATE"
curl -i -X PUT -T /opt/fog-loopback-v2/server/storage/container1/test-video.mov "http://$DATANODE:50075/webhdfs/v1/fulorrgege/Observation/test-video.mov?op=CREATE&user.name=root&namenoderpcaddress=$NAMENODE:8020&overwrite=false"
curl -i -X PUT "http://$NAMENODE:50070/webhdfs/v1/fulorrgege/Observation/test-video1.mov?user.name=root&op=CREATE"
curl -i -X PUT -T /opt/fog-loopback-v2/server/storage/container1/test-video1.mov "http://$DATANODE:50075/webhdfs/v1/fulorrgege/Observation/test-video1.mov?op=CREATE&user.name=root&namenoderpcaddress=$NAMENODE:8020&overwrite=false"
rm /opt/fog-loopback-v2/server/storage/container1/test-pic.jpg
rm /opt/fog-loopback-v2/server/storage/container1/test-video.mov
rm /opt/fog-loopback-v2/server/storage/container1/test-pic1.jpg
rm /opt/fog-loopback-v2/server/storage/container1/test-video1.mov
node /opt/fog-loopback-v2/
