# Hadoop, Cassandra and Loopback system in Docker-Compose

Requirements: 
- Docker
## Docker SWARM cluster
### Start virtual system:
```sh
$ cd ./health_team
$ docker stack deploy --compose-file docker-compose-swarm.yml distributed
```
### Stop virtual system:
```sh
$ docker stack rm distributed
```
## Docker container
### Start virtual system:
```sh
$ cd ./health_team
$ docker-compose up -d
```
### Stop virtual system:
```sh
$ docker-compose down
```
### HDFS directly
Configure the attached hosts file (ask Zsombor) and overwrite your own hosts file with the attached one.
### Ports
 - Cassandra:
 	- 5083:9042
    - 5084:7199
    - 5085:9160
    - 5086:7000
    - 5007:7001
 - Hadoop NameNode:
    - 50070:50070
    - 50080:8020
 - Hadoop DataNode:
    - 50081:50010
    - 50082:50020
    - 50075:50075
    
Usage:
available port:forwarded port from a container

Zsombor Hunyadi