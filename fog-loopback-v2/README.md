# Otolaryngology loopback


### Description
You are able to use this server for transfer FHIR resources from client to Cassandra and media files to HDFS.  
[Loopback 2.x](https://loopback.io/doc/en/lb2/index.html "Loopback 2.x")

This application is connecting to Cassandra and HDFS datasources with the following connectors:  
[Cassandra connector](https://www.npmjs.com/package/loopback-connector-cassandra "Cassandra connector")  
[Rest connector (HDFS)](https://www.npmjs.com/package/loopback-connector-rest "Rest connector (HDFS)")  



We are using fhir standards: [https://www.hl7.org/fhir/resourcelist.html](https://loopback.io/doc/en/lb2/index.html "https://www.hl7.org/fhir/resourcelist.html")

### Table of Contents
1. [Installion](#installion)
2. [Usage](#usage)
3. [Credits](#credits)
4. [License](#license)


### Installation <a name="installion"></a>
Running:

```bash
cd [repository name]
```
```bash
$ npm install
$ node .
```


### Usage <a name="usage"></a>

*./examples* - example json-s for the interfaces  
*./swagger-spec* - swagger editor files for open Api standards

FHIR Models: [https://www.hl7.org/fhir/resourcelist.html](https://loopback.io/doc/en/lb2/index.html "https://www.hl7.org/fhir/resourcelist.html")

**./common/models/**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*container.js* - container model for file operations  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*diagnostic-report.js* - CRUD operations for FHIR DiagnosticReport model  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*email.js* - email sender service  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*measurement.js* - this model managing the file share between the client, loopback and hdfs  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*observation.js* - CRUD operations for FHIR Observation model

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*patient.js* - CRUD operations for FHIR Patient model

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*practitioner.js* - CRUD operations for FHIR Practitioner model

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*questionnaire-response.js* - CRUD operations for FHIR Questionnaire Response model

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*questionnaire.js* - CRUD operations for FHIR Questionnaire model

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*referral-request.js* - CRUD operations for FHIR Referral Request model

### Credits <a name="credits"></a>  
[attilavarga96](https://github.com/attilavarga96 "attilavarga96")  
Miroslav Brnic


### License <a name="license"></a>
MIT
