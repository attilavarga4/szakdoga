'use strict';

var http = require('http');
var request = require('request');
var fs = require('fs');

module.exports = function (Measurement) {

    Measurement.uploadMedia = function (id, args, cb) {
        var hdfsService = Measurement.app.dataSources.hdfs;
        var storageService = Measurement.app.dataSources.storage;
        hdfsService.save(this.definition.name + '/' + id,
            function (err, response, context) {
                var r = request.put(context.headers.location);
                var upload = fs.createReadStream(storageService.settings.root + '/container1/' + id);
                upload.pipe(r);
                if (err) {
                    console.log(err, "Error");
                    cb(err);
                } else {
                    console.log(response, "Success");
                    cb(null, response);
                }
            });
    };

    Measurement.readMedia = function (id, cb) {
        var hdfsService = Measurement.app.dataSources.hdfs;
        var storageService = Measurement.app.dataSources.storage;
        var defaultUri = hdfsService.settings.operations[0].template.url;
        var splitDefaultUri = defaultUri.split('{filename}');
        var uri = splitDefaultUri[0] + this.definition.name + '/' + id + splitDefaultUri[1];
        var r = request(uri);
        r.on('response', function (resp) {
            resp.pipe(fs.createWriteStream(storageService.settings.root + '/container1/' + id));
            cb(null, 'LoopBack downloaded the file.');
        });
    };

    Measurement.deleteMedia = function (id, cb) {
        var hdfsService = Measurement.app.dataSources.hdfs;
        hdfsService.delete(this.definition.name + '/' + id,
            function (err, response, context) {
                if (err) {
                    cb(err);
                } else {
                    cb(null, response);
                }
            });
    };

    Measurement.setup = function () {
        Measurement.base.setup.call(this);
        var MeasurementModel = this;

        MeasurementModel.remoteMethod('uploadMedia', {
            accepts: [{ arg: 'id', type: 'string', required: true }, { arg: 'data', type: 'object', http: { source: 'body' } }],
            http: { path: '/:id/upload-media', verb: 'put' },
            returns: { arg: 'result', type: 'string' }
        });

        MeasurementModel.remoteMethod('readMedia', {
            accepts: [{ arg: 'id', type: 'string', required: true }],
            http: { path: '/:id/read-media', verb: 'get' },
            returns: { arg: 'result', type: 'string' }
        });

        MeasurementModel.remoteMethod('deleteMedia', {
            accepts: [{ arg: 'id', type: 'string', required: true }],
            http: { path: '/:id/delete-media', verb: 'delete' },
            returns: { arg: 'result', type: 'string' }
        });
    };

};
