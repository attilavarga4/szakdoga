
module.exports = function (Observation) {

  /**
   * addObservation
   * @param {Observation} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Observation.addObservation = function (body, callback) {

    let id;
    var cassandra = Observation.app.dataSources.cassandra.connector;
    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Observation\\"*'
      + '"}}\')',
      function (err, data) {
        if (err) {
          return callback(err);
        }

        if (data.length == 0) {
          id = 1;
        } else {
          id = Number(data[data.length - 1].number_id) + 1;

        }

        var queryStart = new Date().toISOString();
        JSON.parse(body.content).meta.lastUpdated = new Date().toISOString();
        let content = JSON.parse(body.content);
        content.id = body.resource_name + '_' + id;
        body.content = JSON.stringify(content);
        body.number_id = id;

        var params = [
          body.resource_name,
          body.number_id,
          (body.version ? body.version : 1),
          (body.resource_type ? body.resource_type : 'Observation'),
          (body.active ? body.active : 'active'),
          queryStart,
          'json',
          body.author,
          body.content
        ];
        console.log(params);
        cassandra.execute('INSERT INTO FHIR_RESOURCES ("resource_name", "number_id", "version", "resource_type", "state", "lastupdated", "format", "author", "content") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', params,
          function (err2) {
            if (err2) {
              return callback(err2);
            }
            callback(null, body, 'application/json');
          });
      });
  }


  /**
   * getObservation
   * @param {string} resource_id exp.: obs_1
   * @param {string} basedOn Reference to the ReferralRequest.
   * @param {string} status registered | preliminary | final
   * @param {string} code LOINC
   * @param {string} subject This should be the patient.
   * @param {string} issued Date
   * @param {string} performer This should be the practitioner.
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {Observation} result Result object
   */
  Observation.getObservation = function (resource_id, basedOn, status, code, subject, issued, performer, callback) {

    var cassandra = Observation.app.dataSources.cassandra.connector;

    resource_id ? resource_id = resource_id.split('_') : '';

    console.log('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + ' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Observation\\"*'
      + (basedOn ? 'basedOn\\":[{\\"reference\\":\\"ReferralRequest\\/' + basedOn + '\\"}]*' : '')
      + (status ? 'status\\":\\"' + status + '*' : '')
      + (subject ? 'subject\\":[*{\\"reference\\":\\"Patient\\/' + subject + '\\"}*]*' : '')
      + (issued ? 'issued\\":\\"' + issued + '*' : '')
      + (performer ? 'performer\\":[{\\"reference\\":\\"Practitioner\\/' + performer + '\\"}]*' : '')
      + (code ? '\\"code\\":{\\"coding\\":[*{\\"system\\":\\"http://loinc.org\\",\\"code\\":\\"' + code + '\\",\\"display\\":\\"*\\"}*]}*' : '')
      //+ (component_code ? 'component\\":[{*\\"code\\":{\\"coding\\":[{\\"system\\":\\"http://loinc.org\\",\\"code\\":\\"'+ component_code +'\\",\\"display\\":\\"*\\"}]}*}]*' : '')
      + '\"}'
      //+ ', sort:{fields: [{field:\"resource_id\", reverse:false}]}'
      + '}\')');
    //CONSOLE//CONSOLE//CONSOLE//CONSOLE//CONSOLE//CONSOLE

    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + ' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Observation\\"*'
      + (basedOn ? 'basedOn\\":[{\\"reference\\":\\"ReferralRequest\\/' + basedOn + '\\"}]*' : '')
      + (status ? 'status\\":\\"' + status + '*' : '')
      + (subject ? 'subject\\":[*{\\"reference\\":\\"Patient\\/' + subject + '\\"}*]*' : '')
      + (issued ? 'issued\\":\\"' + issued + '*' : '')
      + (performer ? 'performer\\":[{\\"reference\\":\\"Practitioner\\/' + performer + '\\"}]*' : '')
      + (code ? '\\"code\\":{\\"coding\\":[*{\\"system\\":\\"http://loinc.org\\",\\"code\\":\\"' + code + '\\",\\"display\\":\\"*\\"}*]}*' : '')
      //+ (component_code ? 'component\\":[{*\\"code\\":{\\"coding\\":[{\\"system\\":\\"http://loinc.org\\",\\"code\\":\\"'+ component_code +'\\",\\"display\\":\\"*\\"}]}*}]*' : '')
      + '\"}'
      //+ ', sort:{fields: [{field:\"resource_id\", reverse:false}]}'
      + '}\')',
      function (err, data) {
        console.log("data///////");
        /*if((code && component_code) || component_code){
          if(component_code != code){
            if(component_code){
              var searchTerm = component_code;
              var data = data.filter(function(row) {
                return row.content.indexOf(searchTerm) > -1;
              });
            }
          } else {
            err = new Error('The code and code_component should not be the same.');
          }
        }*/

        /*data.sort(function(a, b) {
          var idA = a.resource_id.split("_");
          var idB = b.resource_id.split("_");
          if (Number(idA[1]) < Number(idB[1])) {
            return -1;
          }
          if (Number(idA[1]) > Number(idB[1])) {
            return 1;
          }
        
          return 0;
        });*/
        console.log(data);
        console.log(err);

        callback(err, data);
      });

  }


  /**
   * deleteObservation
   * @param {string} resource_id exp.: obs_1
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Observation.deleteObservation = function (resource_id, callback) {

    var cassandra = Observation.app.dataSources.cassandra.connector;

    resource_id ? resource_id = resource_id.split('_') : '';

    cassandra.execute('DELETE FROM FHIR_RESOURCES WHERE resource_name=? AND number_id=?', resource_id,
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, resource_id[0] + '_' + resource_id[1] + ' Observation was successfully deleted.');
      });

  }


  /**
   * updateObservation
   * @param {string} resource_id exp.: obs_1
   * @param {Observation} body Able to change author and content.
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Observation.updateObservation = function (resource_id, body, callback) {

    var cassandra = Observation.app.dataSources.cassandra.connector;
    resource_id = resource_id.split('_');
    var queryStart = new Date().toISOString();
    var content = JSON.parse(body.content);

    content.id = resource_id[0] + '_' + resource_id[1];
    content.meta.lastUpdated = queryStart;

    // a response miatt kell ez a három sor
    body.number_id = resource_id[1];
    body.lastupdated = queryStart;
    body.content = JSON.stringify(content);

    console.log('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + queryStart + '\', author = \'' + body.author
      + '\', content = \'' + JSON.stringify(content) + '\' WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] : ''));

    cassandra.execute('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + queryStart + '\', author = \'' + body.author
      + '\', content = \'' + JSON.stringify(content) + '\' WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] : ''),
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, body);
      });

  }

  Observation.sendEmail = function (msg, msg2, to, cb) {
    Observation.app.models.Email.send({
      to: to,
      from: 'wlab.devices@gmail.com',
      subject: msg,
      text: 'my text',
      html: msg2
    }, function (err, mail) {
      console.log('email sent!');
      cb(err);
    });
  };

  Observation.remoteMethod('sendEmail', {
    accepts: [{ arg: 'msg', type: 'string', required: true }, { arg: 'msg2', type: 'string', required: true }, { arg: 'to', type: 'string', required: true }],
    http: { path: '/send-email', verb: 'put' },
    returns: { arg: 'result', type: 'string' }
  });

  Observation.remoteMethod('addObservation',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Observation',
          description: undefined,
          required: true,
          http: { source: 'body' }
        }],
      returns: [{
        description: 'Created',
        type: ['Observation'],
        arg: 'data',
        root: true
      }],
      http: { verb: 'post', path: '', status: 201, errorStatus: 400 },
      description: undefined
    }
  );

  Observation.remoteMethod('getObservation',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'exp.: obs_1',
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'basedOn',
          type: 'string',
          description: 'Reference to the ReferralRequest.',
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'status',
          type: 'string',
          description: 'registered | preliminary | final',
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'code',
          type: 'string',
          description: 'LOINC',
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'subject',
          type: 'string',
          description: 'This should be the patient.',
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'issued',
          type: 'string',
          description: 'Date',
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'performer',
          type: 'string',
          description: 'This should be the practitioner.',
          required: false,
          http: { source: 'query' }
        }],
      returns:
        [{
          description: 'OK',
          type: ['Observation'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Observation.remoteMethod('deleteObservation',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'exp.: obs_1',
          required: true,
          http: { source: 'query' }
        }],
      returns: [
        {
          description: 'OK',
          type: ['Observation'],
          arg: 'data',
          root: true
        }
      ],
      http: { verb: 'delete', path: '', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

  Observation.remoteMethod('updateObservation',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'exp.: obs_1',
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'body',
          type: 'Observation',
          description: 'Able to change author and content.',
          required: true,
          http: { source: 'body' }
        }],
      returns: [
        {
          description: 'OK',
          type: ['Observation'],
          arg: 'data',
          root: true
        }
      ],
      http: { verb: 'put', path: '/:resource_id', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

}
