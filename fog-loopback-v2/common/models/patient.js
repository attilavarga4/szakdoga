
module.exports = function (Patient) {

  /**
   * addPatient
   * @param {Patient} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Patient.addPatient = function (body, callback) {

    let id;
    var cassandra = Patient.app.dataSources.cassandra.connector;
    var queryStart = new Date().toISOString();

    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Patient\\"*'
      + '"}}\')',
      function (err, data) {
        if (err) {
          return callback(err);
        }

        if (data.length == 0) {
          id = 1;
        } else {
          id = Number(data[data.length - 1].number_id) + 1;
        }
        let content = JSON.parse(body.content);

        content.meta.lastUpdated = new Date().toISOString();
        content.id = body.resource_name + '_' + id;

        body.content = JSON.stringify(content);
        body.number_id = id;

        var params = [
          body.resource_name,
          body.number_id,
          (body.version ? body.version : 1),
          (body.resource_type ? body.resource_type : 'Patient'),
          (body.active ? body.active : 'active'),
          queryStart,
          'json',
          body.author,
          body.content
        ];
        console.log(params);
        cassandra.execute('INSERT INTO FHIR_RESOURCES ("resource_name", "number_id", "version", "resource_type", "state", "lastupdated", "format", "author", "content") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', params,
          function (err2) {
            if (err2) {
              return callback(err2);
            }
            callback(null, body, 'application/json');
          });
      });

  }


  /**
   * getPatient
   * @param {string} resource_id undefined
   * @param {string} family undefined
   * @param {string} given undefined
   * @param {string} prefix undefined
   * @param {string} suffix undefined
   * @param {string} general_practitioner undefined
   * @param {boolean} active undefined
   * @param {string} gender undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {Patient} result Result object
   */
  Patient.getPatient = function (resource_id, family, given, prefix, suffix, general_practitioner, active, gender, callback) {

    var cassandra = Patient.app.dataSources.cassandra.connector;

    resource_id ? resource_id = resource_id.split('_') : '';

    if (family) {
      var helper = '';
      family = family.split(" ");
      for (var i = 0; i < family.length; i++) {
        helper += family[i] + '*'
      }
      family = helper;
    }
    if (given) {
      var helper = '';
      given = given.split(" ");
      for (var i = 0; i < given.length; i++) {
        helper += given[i] + '*'
      }
      given = helper;
    }
    if (suffix) {
      var helper = '';
      suffix = suffix.split(" ");
      for (var i = 0; i < suffix.length; i++) {
        helper += suffix[i] + '*'
      }
      suffix = helper;
    }
    if (prefix) {
      var helper = '';
      prefix = prefix.split(" ");
      for (var i = 0; i < prefix.length; i++) {
        helper += prefix[i] + '*'
      }
      prefix = helper;
    }

    console.log('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + ' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Patient\\"*'
      + (active ? 'active\\":true*' : '')
      + (active === false ? 'active\\":false*' : '')
      + (family ? 'family\\":[\\"' + family + '*' : '')
      + (given ? 'given\\":[\\"' + given : '')
      + (prefix ? 'prefix\\":[\\"' + prefix + '*' : '')
      + (suffix ? 'suffix\\":[\\"' + suffix + '*' : '')
      + (gender ? 'gender\\":*\\"' + gender + '*' : '')
      + (general_practitioner ? 'practitioner\\":\\"' + general_practitioner + '*' : '')
      + '"}}\')');
    //CONSOLE//CONSOLE//CONSOLE//CONSOLE//CONSOLE//CONSOLE

    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + ' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Patient\\"*'
      + (active ? 'active\\":true*' : '')
      + (active === false ? 'active\\":false*' : '')
      + (family ? 'family\\":[\\"' + family + '*' : '')
      + (given ? 'given\\":[\\"' + given + '*' : '')
      + (prefix ? 'prefix\\":[\\"' + prefix + '*' : '')
      + (suffix ? 'suffix\\":[\\"' + suffix + '*' : '')
      + (gender ? 'gender\\":\\"' + gender + '*' : '')
      + (general_practitioner ? 'practitioner\\":\\"' + general_practitioner + '*' : '')
      + '"}}\')',
      function (err, data) {
        callback(err, data);
      });

  }


  /**
   * deletePatient
   * @param {string} resource_id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Patient.deletePatient = function (resource_id, callback) {

    var cassandra = Patient.app.dataSources.cassandra.connector;

    resource_id ? resource_id = resource_id.split('_') : '';

    cassandra.execute('DELETE FROM FHIR_RESOURCES WHERE resource_name=? AND number_id=?', resource_id,
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, resource_id[0] + '_' + resource_id[1] + ' Patient was successfully deleted.');
      });


  }


  /**
    * updatePatient
    * @param {string} resource_id undefined
    * @param {Patient} body Able to change author and content.
    * @callback {Function} callback Callback function
    * @param {Error|string} err Error object
    * @param {any} result Result object
    */
  Patient.updatePatient = function (resource_id, body, callback) {

    var cassandra = Patient.app.dataSources.cassandra.connector;
    resource_id = resource_id.split('_');
    var queryStart = new Date().toISOString();
    var content = JSON.parse(body.content);

    content.id = resource_id[0] + '_' + resource_id[1];
    content.meta.lastUpdated = queryStart;

    // a response miatt kell ez a három sor
    body.number_id = resource_id[1];
    body.lastupdated = queryStart;
    body.content = JSON.stringify(content);

    console.log('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + queryStart + '\', author = \'' + body.author
      + '\', content = \'' + JSON.stringify(content) + '\' WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] : ''));

    cassandra.execute('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + queryStart + '\', author = \'' + body.author
      + '\', content = \'' + JSON.stringify(content) + '\' WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] : ''),
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, body);
      });

  }




  Patient.remoteMethod('addPatient',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Patient',
          description: undefined,
          required: true,
          http: { source: 'body' }
        }],
        returns: [{
          description: 'Created',
          type: ['Patient'],
          arg: 'data',
          root: true
        }],
        http: { verb: 'post', path: '', status: 201, errorStatus: 400 },
      description: undefined
    }
  );

  Patient.remoteMethod('getPatient',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'pat_1',
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'family',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'given',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'prefix',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'suffix',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'general_practitioner',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'active',
          type: 'boolean',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'gender',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }],
      returns:
        [{
          description: 'OK',
          type: ['Patient'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Patient.remoteMethod('deletePatient',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'pat_1',
          required: true,
          http: { source: 'query' }
        }],
        returns: [
          {
            description: 'OK',
            type: ['Patient'],
            arg: 'data',
            root: true
          }
        ],
        http: { verb: 'delete', path: '', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

  Patient.remoteMethod('updatePatient',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'pat_1',
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'body',
          type: 'Patient',
          description: 'Able to change author and content.',
          required: true,
          http: { source: 'body' }
        }],
        returns: [
          {
            description: 'OK',
            type: ['Patient'],
            arg: 'data',
            root: true
          }
        ],
        http: { verb: 'put', path: '/:resource_id', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

}
