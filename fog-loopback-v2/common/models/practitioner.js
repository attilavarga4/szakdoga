
module.exports = function (Practitioner) {

  /**
  * addPractitioner
  * @param {Practitioner} body undefined
  * @callback {Function} callback Callback function
  * @param {Error|string} err Error object
  * @param {any} result Result object
  */
  Practitioner.addPractitioner = function (body, callback) {

    let id;
    var cassandra = Practitioner.app.dataSources.cassandra.connector;
    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Practitioner\\"*'
      + '"}}\')',
      function (err, data) {

        if (data.length == 0) {
          id = 1;
        } else {
          id = Number(data[data.length - 1].number_id) + 1;

        }

        var queryStart = new Date().toISOString();
        JSON.parse(body.content).meta.lastUpdated = new Date().toISOString();
        let content = JSON.parse(body.content);
        content.id = body.resource_name + '_' + id;
        body.content = JSON.stringify(content);
        body.number_id = id;

        var params = [
          body.resource_name,
          body.number_id,
          (body.version ? body.version : 1),
          (body.resource_type ? body.resource_type : 'Practitioner'),
          (body.active ? body.active : 'active'),
          queryStart,
          'json',
          body.author,
          body.content
        ];
        console.log(params);
        cassandra.execute('INSERT INTO FHIR_RESOURCES ("resource_name", "number_id", "version", "resource_type", "state", "lastupdated", "format", "author", "content") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', params,
          function (err2) {
            if (err2) {
              return callback(err2);
            }
            callback(null, body, 'application/json');
          });
      });
  }


  /**
  * getPractitioner
  * @param {string} resource_id undefined
  * @param {string} family undefined
  * @param {string} given undefined
  * @param {string} prefix undefined
  * @param {string} suffix undefined
  * @param {boolean} active undefined
  * @param {string} qualification undefined
  * @callback {Function} callback Callback function
  * @param {Error|string} err Error object
  * @param {Practitioner} result Result object
  */
  Practitioner.getPractitioner = function (resource_id, family, given, prefix, suffix, active, qualification, callback) {
    resource_id ? resource_id = resource_id.split('_') : '';

    var cassandra = Practitioner.app.dataSources.cassandra.connector;
    if (family) {
      var helper = '';
      family = family.split(" ");
      for (var i = 0; i < family.length; i++) {
        helper += family[i] + '*'
      }
      family = helper;
    }
    if (given) {
      var helper = '';
      given = given.split(" ");
      for (var i = 0; i < given.length; i++) {
        helper += given[i] + '*'
      }
      given = helper;
    }
    if (suffix) {
      var helper = '';
      suffix = suffix.split(" ");
      for (var i = 0; i < suffix.length; i++) {
        helper += suffix[i] + '*'
      }
      suffix = helper;
    }
    if (prefix) {
      var helper = '';
      prefix = prefix.split(" ");
      for (var i = 0; i < prefix.length; i++) {
        helper += prefix[i] + '*'
      }
      prefix = helper;
    }


    console.log('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_id=\'' + resource_id + '\' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Practitioner\\"*'
      + (qualification ? '\\"extension\\":[{\\"url\\":\\"http://hl7.org/fhir/StructureDefinition/practitioner-classification\\",\\"valueCode\\": \\"' + qualification + '\\"}]*' : '')
      + (active ? 'active\\":true*' : '')
      + (active === false ? 'active\\":false*' : '')
      + (family ? 'family\\":[\\"' + family + '*' : '')
      + (given ? 'given\\":[\\"' + given + '*' : '')
      + (prefix ? 'prefix\\":[\\"' + prefix + '*' : '')
      + (suffix ? 'suffix\\":[\\"' + suffix + '*' : '')
      + '"}}\')');
    //CONSOLE//CONSOLE//CONSOLE//CONSOLE//CONSOLE//CONSOLE

    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND ' + 'number_id=' + resource_id[1] + ' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Practitioner\\"*'
      + (qualification ? '\\"extension\\":[{\\"url\\":\\"http://hl7.org/fhir/StructureDefinition/practitioner-classification\\",\\"valueCode\\": \\"' + qualification + '\\"}]*' : '')
      + (active ? 'active\\":true*' : '')
      + (active === false ? 'active\\":false*' : '')
      + (family ? 'family\\":[\\"' + family + '*' : '')
      + (given ? 'given\\":[\\"' + given + '*' : '')
      + (prefix ? 'prefix\\":[\\"' + prefix + '*' : '')
      + (suffix ? 'suffix\\":[\\"' + suffix + '*' : '')
      + '"}}\')',
      function (err, data) {
        callback(err, data);
      });

  }



  /**
  * deletePractitioner
  * @param {string} resource_id undefined
  * @callback {Function} callback Callback function
  * @param {Error|string} err Error object
  * @param {any} result Result object
  */
  Practitioner.deletePractitioner = function (resource_id, callback) {

    var cassandra = Practitioner.app.dataSources.cassandra.connector;

    resource_id ? resource_id = resource_id.split('_') : '';

    cassandra.execute('DELETE FROM FHIR_RESOURCES WHERE resource_name=? AND number_id=?', resource_id,
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, resource_id[0] + '_' + resource_id[1] + ' Practitioner was successfully deleted.');
      });
  }


  /**
  * updatePractitioner
  * @param {string} resource_id undefined
  * @param {Practitioner} body Able to change author and content.
  * @callback {Function} callback Callback function
  * @param {Error|string} err Error object
  * @param {any} result Result object
  */
  Practitioner.updatePractitioner = function (resource_id, body, callback) {

    var cassandra = Practitioner.app.dataSources.cassandra.connector;
    resource_id = resource_id.split('_');
    var queryStart = new Date().toISOString();
    var content = JSON.parse(body.content);

    content.id = resource_id[0] + '_' + resource_id[1];
    content.meta.lastUpdated = queryStart;

    // a response miatt kell ez a három sor
    body.number_id = resource_id[1];
    body.lastupdated = queryStart;
    body.content = JSON.stringify(content);

    console.log('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + queryStart + '\', author = \'' + body.author
      + '\', content = \'' + JSON.stringify(content) + '\' WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] : ''));

    cassandra.execute('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + queryStart + '\', author = \'' + body.author
      + '\', content = \'' + JSON.stringify(content) + '\' WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] : ''),
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, body);
      });

  }




  Practitioner.remoteMethod('addPractitioner',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Practitioner',
          description: undefined,
          required: true,
          http: { source: 'body' }
        }],
      returns: [{
        description: 'Created',
        type: ['Practitioner'],
        arg: 'data',
        root: true
      }],
      http: { verb: 'post', path: '', status: 201, errorStatus: 400 },
      description: undefined
    }
  );

  Practitioner.remoteMethod('getPractitioner',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'pr_1',
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'family',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'given',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'prefix',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'suffix',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'active',
          type: 'boolean',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'qualification',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }],
      returns:
        [{
          description: 'OK',
          type: ['Practitioner'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Practitioner.remoteMethod('deletePractitioner',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'pr_1',
          required: true,
          http: { source: 'query' }
        }],
      returns: [
        {
          description: 'OK',
          type: ['Practitioner'],
          arg: 'data',
          root: true
        }
      ],
      http: { verb: 'delete', path: '', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

  Practitioner.remoteMethod('updatePractitioner',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: 'pr_1',
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'body',
          type: 'Practitioner',
          description: 'Able to change author and content.',
          required: true,
          http: { source: 'body' }
        }],
        returns: [
          {
            description: 'OK',
            type: ['Practitioner'],
            arg: 'data',
            root: true
          }
        ],
      http: { verb: 'put', path: '/:resource_id', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

}
