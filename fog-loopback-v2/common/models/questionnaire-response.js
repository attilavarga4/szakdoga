
module.exports = function (QuestionnaireResponse) {

  /**
   * addQuestionnaireResponse
   * @param {QuestionnaireResponse} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  QuestionnaireResponse.addQuestionnaireResponse = function (body, callback) {
    let id;
    var cassandra = QuestionnaireResponse.app.dataSources.cassandra.connector;
    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"QuestionnaireResponse\\"*'
      + '"}}\')',
      function (err, data) {
        if (err) {
          return callback(err);
        }

        if (data.length == 0) {
          id = 1;
        } else {
          id = Number(data[data.length - 1].number_id) + 1;

        }
        var queryStart = new Date().toISOString();
        let content = JSON.parse(body.content);

        content.meta.lastUpdated = new Date().toISOString();
        content.id = body.resource_name + '_' + id;

        body.content = JSON.stringify(content);
        body.number_id = id;

        var params = [
          body.resource_name,
          body.number_id,
          (body.version ? body.version : 1),
          (body.resource_type ? body.resource_type : 'Questionnaire'),
          (body.active ? body.active : 'active'),
          queryStart,
          'json',
          body.author,
          body.content
        ];
        // console.log(params)
        cassandra.execute('INSERT INTO FHIR_RESOURCES ("resource_name","number_id", "version", "resource_type", "state", "lastupdated", "format", "author", "content") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', params,
          function (err2) {
            if (err2) {
              return callback(err2);
            }
            callback(null, body, 'application/json');
          });
      });

  }


  /**
   * getQuestionnaireResponse
   * @param {string} resource_id undefined
   * @param {string} status undefined
   * @param {string} text undefined
   * @param {string} answer undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {QuestionnaireResponse} result Result object
   */
  QuestionnaireResponse.getQuestionnaireResponse = function (resource_id, status, text, answer, callback) {

    resource_id ? resource_id = resource_id.split('_') : '';
    var cassandra = QuestionnaireResponse.app.dataSources.cassandra.connector;

    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + ' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"QuestionnaireResponse\\"*'
      + (status ? 'status\\":\\"' + status + '*' : '')
      + (text ? '*text\\":\\"*' + text + '*' : '')
      + (answer ? '*answer\\":[{\\"*' + answer + '*' : '')
      + '"}}\')',
      function (err, data) {
        callback(err, data);
      });



  }


  /**
   * deleteQuestionnaireResponse
   * @param {string} resource_id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  QuestionnaireResponse.deleteQuestionnaireResponse = function (resource_id, callback) {

    resource_id ? resource_id = resource_id.split('_') : '';

    var cassandra = QuestionnaireResponse.app.dataSources.cassandra.connector;


    cassandra.execute('DELETE FROM FHIR_RESOURCES WHERE resource_name = ? AND number_id = ?', resource_id,
    function (err) {
      if (err) {
        callback(err);
      }

      callback(null, resource_id[0] + '_' + resource_id[1] + ' QuestionnaireResponse was successfully deleted.');
    });
  }


  /**
   * updateQuestionnaireResponse
   * @param {string} resource_id undefined
   * @param {QuestionnaireResponse} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  QuestionnaireResponse.updateQuestionnaireResponse = function (resource_id, body, callback) {

    var cassandra = QuestionnaireResponse.app.dataSources.cassandra.connector;
    resource_id ? resource_id = resource_id.split('_') : '';

    var queryStart = new Date().toISOString();
    var content = JSON.parse(body.content);

    content.id = resource_id[0] + '_' + resource_id[1];
    content.meta.lastUpdated = queryStart;

    // a response miatt kell ez a három sor
    body.number_id = resource_id[1];
    body.lastupdated = queryStart;
    body.content = JSON.stringify(content);

    cassandra.execute('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + body.lastupdated
      + '\', content = \'' + JSON.stringify(content) + '\'' + ' WHERE resource_name=\''
      + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + '',
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, body);
      });

  }




  QuestionnaireResponse.remoteMethod('addQuestionnaireResponse',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'QuestionnaireResponse',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }],
        returns: [{
          description: 'Created',
          type: ['QuestionnaireResponse'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'post', path: '', status: 201, errorStatus: 400 },
      description: undefined
    }
  );

  QuestionnaireResponse.remoteMethod('getQuestionnaireResponse',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'status',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'text',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'answer',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }],
      returns:
        [{
          description: 'OK',
          type: ['QuestionnaireResponse'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  QuestionnaireResponse.remoteMethod('deleteQuestionnaireResponse',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }],
        returns: [
          {
            description: 'OK',
            type: ['QuestionnaireResponse'],
            arg: 'data',
            root: true
          }
        ],
      http: { verb: 'delete', path: '', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

  QuestionnaireResponse.remoteMethod('updateQuestionnaireResponse',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'body',
          type: 'QuestionnaireResponse',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }],
        returns: [
          {
            description: 'OK',
            type: ['QuestionnaireResponse'],
            arg: 'data',
            root: true
          }
        ],
      http: { verb: 'put', path: '/:resource_id', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

}
