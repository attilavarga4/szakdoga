
module.exports = function (Questionnaire) {

  /**
   * addQuestionnaire
   * @param {Questionnaire} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Questionnaire.addQuestionnaire = function (body, callback) {
    let id;
    var cassandra = Questionnaire.app.dataSources.cassandra.connector;
    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Questionnaire\\"*'
      + '"}}\')',
      function (err, data) {
        if (err) {
          return callback(err);
        }

        if (data.length == 0) {
          id = 1;
        } else {
          id = Number(data[data.length - 1].number_id) + 1;

        }



        var queryStart = new Date().toISOString();
        JSON.parse(body.content).meta.lastUpdated = new Date().toISOString();

        let content = JSON.parse(body.content);
        content.id = body.resource_name + '_' + id;
        body.content = JSON.stringify(content);
        body.number_id = id;

        var params = [
          body.resource_name,
          body.number_id,
          (body.version ? body.version : 1),
          (body.resource_type ? body.resource_type : 'Questionnaire'),
          (body.active ? body.active : 'active'),
          queryStart,
          'json',
          body.author,
          body.content
        ];
        cassandra.execute('INSERT INTO FHIR_RESOURCES ("resource_name","number_id", "version", "resource_type", "state", "lastupdated", "format", "author", "content") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', params,
          function (err2) {
            if (err2) {
              return callback(err2);
            }
            callback(null, body, 'application/json');
          });
      });

  }


  /**
   * getQuestionnaire
   * @param {string} resource_id undefined
   * @param {string} status undefined
   * @param {string} code undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {Questionnaire} result Result object
   */
  Questionnaire.getQuestionnaire = function (resource_id, status, code, callback) {
    resource_id ? resource_id = resource_id.split('_') : '';
    var cassandra = Questionnaire.app.dataSources.cassandra.connector;

    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + ' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"Questionnaire\\"*'
      + (status ? 'status\\":\\"' + status + '*' : '')
      + (code ? '*code\\":\\"*' + code + '*' : '')
      + '"}}\')',
      function (err, data) {
        callback(err, data);
      });


  }










  /**
   * deleteQuestionnaire
   * @param {string} resource_id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Questionnaire.deleteQuestionnaire = function (resource_id, callback) {
    resource_id ? resource_id = resource_id.split('_') : '';

    var cassandra = Questionnaire.app.dataSources.cassandra.connector;


    cassandra.execute('DELETE FROM FHIR_RESOURCES WHERE resource_name = ? AND number_id = ?', resource_id,
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, resource_id[0] + '_' + resource_id[1] + ' Questionnaire was successfully deleted.');
      });

  }


  /**
   * updateQuestionnaire
   * @param {string} resource_id undefined
   * @param {string} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Questionnaire.updateQuestionnaire = function (resource_id, body, callback) {
    var cassandra = Questionnaire.app.dataSources.cassandra.connector;
    resource_id ? resource_id = resource_id.split('_') : '';
    var queryStart = new Date().toISOString();
    var content = JSON.parse(body.content);

    content.id = resource_id[0] + '_' + resource_id[1];
    content.meta.lastUpdated = queryStart;

    // a response miatt kell ez a három sor
    body.number_id = resource_id[1];
    body.lastupdated = queryStart;
    body.content = JSON.stringify(content);

    cassandra.execute('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + body.lastupdated
      + '\', content = \'' + JSON.stringify(content) + '\'' + ' WHERE resource_name=\''
      + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + '',
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, body);
      });

  }




  Questionnaire.remoteMethod('addQuestionnaire',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Questionnaire',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }],
        returns: [{
          description: 'Created',
          type: ['Questionnaire'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'post', path: '', status: 201, errorStatus: 400 },
      description: undefined
    }
  );

  Questionnaire.remoteMethod('getQuestionnaire',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'status',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'code',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }
        ],
      returns:
        [{
          description: 'OK',
          type: ['Questionnaire'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Questionnaire.remoteMethod('deleteQuestionnaire',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }],
        returns: [
          {
            description: 'OK',
            type: ['Questionnaire'],
            arg: 'data',
            root: true
          }
        ],
      http: { verb: 'delete', path: '', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

  Questionnaire.remoteMethod('updateQuestionnaire',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'body',
          type: 'Questionnaire',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        },
        ],
        returns: [
          {
            description: 'OK',
            type: ['Questionnaire'],
            arg: 'data',
            root: true
          }
        ],
      http: { verb: 'put', path: '/:resource_id', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

}
