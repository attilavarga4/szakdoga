
module.exports = function (ReferralRequest) {

  /**
   * addReferralRequest
   * @param {ReferralRequest} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  ReferralRequest.addReferralRequest = function (body, callback) {

    let id;
    var cassandra = ReferralRequest.app.dataSources.cassandra.connector;
    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"ReferralRequest\\"*'
      + '"}}\')',
      function (err, data) {
        if (err) {
          return callback(err);
        }

        if (data.length == 0) {
          id = 1;
        } else {
          id = Number(data[data.length - 1].number_id) + 1;

        }
        var queryStart = new Date().toISOString();
        let content = JSON.parse(body.content);

        content.id = body.resource_name + '_' + id;
        content.meta.lastUpdated = new Date().toISOString();

        body.content = JSON.stringify(content);
        body.number_id = id;

        var params = [
          body.resource_name,
          body.number_id,
          (body.version ? body.version : 1),
          (body.resource_type ? body.resource_type : 'Questionnaire'),
          (body.active ? body.active : 'active'),
          queryStart,
          'json',
          body.author,
          body.content
        ];
        // console.log(params)
        cassandra.execute('INSERT INTO FHIR_RESOURCES ("resource_name","number_id", "version", "resource_type", "state", "lastupdated", "format", "author", "content") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', params,
          function (err2) {
            if (err2) {
              return callback(err2);
            }
            callback(null, body, 'application/json');
          });
      });


  }


  /**
   * getReferralRequest
   * @param {string} resource_id undefined
   * @param {string} status undefined
   * @param {string} intent undefined
   * @param {string} patient undefined
   * @param {string} authored_on undefined
   * @param {string} priority undefined
   * @param {string} requester undefined
   * @param {string} recipient undefined
   * @param {string} basedOn undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {ReferralRequest} result Result object
   */
  ReferralRequest.getReferralRequest = function (resource_id, status, intent, patient, authored_on, priority, requester, recipient, basedOn, callback) {
    var cassandra = ReferralRequest.app.dataSources.cassandra.connector;
    resource_id ? resource_id = resource_id.split('_') : '';

    /*
     console.log('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_id=\'' + resource_id + '\' AND ' : '') 
     + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"ReferralRequest\\"*' 
     + (basedOn ? 'basedOn\\":[{\\"reference\\":\\"ReferralRequest/'+basedOn+'*' : '')
     + (status ? 'status\\":\\"'+status+'*' : '')
     + (intent ? 'intent\\":\\"'+intent+'*' : '')
     + (patient ? 'subject\\":{\\"reference\\":\\"Patient/'+patient+'*' : '')
     + (authored_on ? 'authoredOn\\":\\"'+authored_on+'*' : '')
     + (requester ? 'requester\\":{\\"agent\\":{\\"reference\\":\\"Practitioner/'+requester+'*' : '')
     + (recipient ? 'recipient\\":[{\\"reference\\":\\"Practitioner/'+recipient+'*' : '')
     + (priority ? 'priority\\":\\"'+priority+'*' : '')
     + '"}}\')');
     //CONSOLE//CONSOLE//CONSOLE//CONSOLE//CONSOLE//CONSOLE
    
   */

    cassandra.execute('SELECT * FROM FHIR_RESOURCES WHERE ' + (resource_id ? 'resource_name=\'' + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + ' AND ' : '')
      + 'expr(idx_obs, \'{filter: {type: "wildcard", field: "content", value: "*\\"resourceType\\":\\"ReferralRequest\\"*'
      + (basedOn ? 'basedOn\\":[{\\"reference\\":\\"ReferralRequest/' + basedOn + '*' : '')
      + (status ? 'status\\":\\"' + status + '*' : '')
      + (intent ? 'intent\\":\\"' + intent + '*' : '')
      + (patient ? 'subject\\":{\\"reference\\":\\"Patient/' + patient + '*' : '')
      + (authored_on ? 'authoredOn\\":\\"' + authored_on + '*' : '')
      + (requester ? 'requester\\":{\\"agent\\":{\\"reference\\":\\"Practitioner/' + requester + '*' : '')
      + (recipient ? 'recipient\\":[{\\"reference\\":\\"Practitioner/' + recipient + '*' : '')
      + (priority ? 'priority\\":\\"' + priority + '*' : '')
      + '"}}\')',
      function (err, data) {
        callback(err, data);
      });



  }


  /**
   * deleteReferralRequest
   * @param {string} resource_id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  ReferralRequest.deleteReferralRequest = function (resource_id, callback) {
    resource_id ? resource_id = resource_id.split('_') : '';

    var cassandra = ReferralRequest.app.dataSources.cassandra.connector;

    cassandra.execute('DELETE FROM FHIR_RESOURCES WHERE resource_name = ? AND number_id = ?', resource_id,
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, resource_id[0] + '_' + resource_id[1] + ' ReferralRequest was successfully deleted.');
      });


  }


  /**
   * updateReferralRequest
   * @param {string} resource_id undefined
   * @param {string} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  ReferralRequest.updateReferralRequest = function (resource_id, body, callback) {
    var cassandra = ReferralRequest.app.dataSources.cassandra.connector;
    resource_id ? resource_id = resource_id.split('_') : '';
    var queryStart = new Date().toISOString();
    var content = JSON.parse(body.content);

    content.id = resource_id[0] + '_' + resource_id[1];
    content.meta.lastUpdated = queryStart;

    // a response miatt kell ez a három sor
    body.number_id = resource_id[1];
    body.lastupdated = queryStart;
    body.content = JSON.stringify(content);

    cassandra.execute('UPDATE FHIR_RESOURCES SET' + ' lastupdated = \'' + body.lastupdated
      + '\', content = \'' + JSON.stringify(content) + '\'' + ' WHERE resource_name=\''
      + resource_id[0] + '\' AND' + ' number_id=' + resource_id[1] + '',
      function (err) {
        if (err) {
          callback(err);
        }

        callback(null, body);
      });


  }




  ReferralRequest.remoteMethod('addReferralRequest',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'ReferralRequest',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }],
      returns: [{
        description: 'Created',
        type: ['ReferralRequest'],
        arg: 'data',
        root: true
      }],
      http: { verb: 'post', path: '', status: 201, errorStatus: 400 },
      description: undefined
    }
  );

  ReferralRequest.remoteMethod('getReferralRequest',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'status',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'intent',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'patient',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'authored_on',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'priority',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'requester',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'recipient',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'basedOn',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        ],
      returns:
        [{
          description: 'OK',
          type: ['ReferralRequest'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  ReferralRequest.remoteMethod('deleteReferralRequest',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }],
        returns: [
          {
            description: 'OK',
            type: ['ReferralRequest'],
            arg: 'data',
            root: true
          }
        ],
      http: { verb: 'delete', path: '', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

  ReferralRequest.remoteMethod('updateReferralRequest',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'resource_id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'body',
          type: 'ReferralRequest',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }],
        returns: [
          {
            description: 'OK',
            type: ['ReferralRequest'],
            arg: 'data',
            root: true
          }
        ],
      http: { verb: 'put', path: '/:resource_id', status: 200, errorStatus: 400 },
      description: undefined
    }
  );

}
